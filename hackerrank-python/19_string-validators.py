if __name__ == '__main__':
    s = raw_input()
    any_alphanum = False
    any_alpha = False
    any_digit = False
    any_lower = False
    any_upper = False

    for i in range(0, len(s)):
        if s[i].isalnum():
            any_alphanum = True
        if s[i].isalpha():
            any_alpha = True
        if s[i].isdigit():
            any_digit = True
        if s[i].islower():
            any_lower = True
        if s[i].isupper():
            any_upper = True

    print(any_alphanum)
    print(any_alpha)
    print(any_digit)
    print(any_lower)
    print(any_upper)
