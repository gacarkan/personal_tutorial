if __name__ == '__main__':
    x = int(raw_input())
    y = int(raw_input())
    z = int(raw_input())
    n = int(raw_input())

    # initialize coords
    coords = list()

    for i_x in range(0, x + 1):
        for i_y in range(0, y + 1):
            for i_z in range(0, z + 1):
                if (i_x + i_y + i_z != n):
                    coords.append([i_x, i_y, i_z])

    print(coords)
