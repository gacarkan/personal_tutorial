from itertools import permutations

args = input().split()
str_in = args[0]
len_in = int(args[1])
out = list(map("".join, permutations(str_in, len_in)))
out.sort()
for i in range(0, len(out)):
    print(out[i])