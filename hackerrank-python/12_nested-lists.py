from operator import itemgetter

if __name__ == '__main__':
    students = list()
    for _ in range(int(input())):
        name = input()
        score = float(input())
        students.append((name, score))

    students.sort(key=itemgetter(1, 0))

    found = False
    found_val = students[0][1]

    for i in range(1, len(students)):
        if students[0][1] < students[i][1]:
            if found == False or (found == True and students[i][1] == students[i - 1][1]):
                if found == False or found_val == students[i][1]:
                    print(students[i][0])
                    found = True
                    found_val = students[i][1]
                else:
                    continue


