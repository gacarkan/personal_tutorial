from collections import Counter

num_shoes = int(input())
input_sizes = input()
sizes = input_sizes.split(' ')
counter_sizes = Counter(sizes)

num_custs = int(input())
revenue = 0

for i in range(0, num_custs):
    input_order = input()
    order = input_order.split(' ')
    if counter_sizes[order[0]] != None and counter_sizes[order[0]] != 0:
        revenue += int(order[1])
        counter_sizes[order[0]] = counter_sizes[order[0]] - 1

print(revenue)
