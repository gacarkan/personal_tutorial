from itertools import combinations

args = input().split(' ')
in_str = sorted(args[0])
in_len = int(args[1])
for i in range(1, in_len + 1):
    out = list(map("".join, combinations(in_str, i)))
    for ii in range(0, len(out)):
        print(out[ii])
